package com.interfaces;

public interface Exercise {
    void performExercise();
}
