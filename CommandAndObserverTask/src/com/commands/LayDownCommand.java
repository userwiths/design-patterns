package com.commands;

import com.company.Trainee;
import com.interfaces.Exercise;

public class LayDownCommand extends TraineeCommand {
    public LayDownCommand(Trainee trainee) {
    	super(trainee);
    }

    @Override
    public void performExercise() {
        this.getTrainee().performLayDown();
    }
}
