package com.commands;

import com.company.Trainee;
import com.interfaces.Exercise;

public abstract class TraineeCommand implements Exercise{
	private Trainee trainee;

	public Trainee getTrainee() {
		return this.trainee;
	}
	
    public TraineeCommand(Trainee trainee) {
        this.trainee = trainee;
    }
}
