package com.commands;

import com.company.Trainee;
import com.interfaces.Exercise;

public class GetUpCommand extends TraineeCommand {
    public GetUpCommand(Trainee trainee) {
        super(trainee);
    }

    @Override
    public void performExercise() {
        this.getTrainee().performGetUp();
    }
}
