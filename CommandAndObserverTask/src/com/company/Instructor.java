package com.company;

import java.util.HashMap;

import com.commands.GetUpCommand;
import com.commands.LayDownCommand;
import com.commands.TraineeCommand;

public class Instructor {
	private HashMap<String,TraineeCommand> commands;

    public Instructor() {
    	this.commands=new HashMap<String,TraineeCommand>();    	
    }
    
    public void setCommand(String name,TraineeCommand command) {
        this.commands.put(name, command);
    }

    public void executeCommand(String name) {
        System.out.println("Instructor says "+name+"!\n");
        this.commands.get(name).performExercise();
    }
}
