package com.company;


import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Trainee extends Observable {
	private String changeableData;
    private List<OnlineViewer> onlineViewers = new ArrayList<OnlineViewer>();

    public void performLayDown() {
    	changeableData="Lay Down";
        setChanged();
        notifyObservers(changeableData);
    }

    public void performGetUp() {
    	changeableData="Get Up";
        setChanged();
        notifyObservers(changeableData);
    }
}
