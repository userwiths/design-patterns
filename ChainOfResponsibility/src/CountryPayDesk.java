public class CountryPayDesk extends PackageHandler {

    @Override
    public void handlePackage(String destination) throws InterruptedException {
        if(destination.equals("Country")) {
            System.out.println("Your country shipment shall be processed as soon as possible");
            this.setPreparationState();
            notifyObservers();
        } else if(successor != null){
            successor.handlePackage(destination);
            this.setWaitingState();
        }
    }
}
