public class AbroadPayDesk extends PackageHandler {
    @Override
    public void handlePackage(String destination) throws InterruptedException {
        if(destination.equals("Abroad")) {
            System.out.println("Your Abroad shipment shall be processed as soon as possible.");
            this.setPreparationState();
            notifyObservers();
        } else {
            System.out.println("We are unable to process your shipment");
            this.setWaitingState();
        }
    }
}
