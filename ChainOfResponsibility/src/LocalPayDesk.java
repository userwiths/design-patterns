public class LocalPayDesk extends PackageHandler {

    @Override
    public void handlePackage(String destination) throws InterruptedException {
        if(destination.equals("Local")) {
            System.out.println("Your local shipment will be processed as soon as possible.");
            this.setPreparationState();
            notifyObservers();
        } else if(successor != null){
            successor.handlePackage(destination);
            this.setWaitingState();
        }
    }
}
