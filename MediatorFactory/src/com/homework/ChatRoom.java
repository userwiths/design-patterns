package com.homework;

import java.util.HashMap;

public class ChatRoom extends AChatRoom {

    private HashMap<String, BaseParticipant> participants = new HashMap<String, BaseParticipant>();
    private ParticipantFactory participantFactory;
    private BaseParticipant botParticipant;

    private void notifyParticipants() {
        participants.forEach((key, value) -> {
            value.receive(botParticipant.getName(), "'cat' has been deemed a forbiden word. Please follow the rules " + key);
        });
    }

    @Override
    public void register(BaseParticipant participant) {
        if(!participants.containsValue(participant)) {
            participants.put(participant.getName(), participant);
        }

        participant.setChatRoom(this);
    }

    @Override
    public void unregister(BaseParticipant participant) {
        if(participants.containsKey(participant.getName())) {
            System.out.printf("Removing %s", participant.getName());
            System.out.println();
            participants.remove(participant.getName());
        }
    }

    @Override
    public void send(String from, String to, String message) {
        if (message == "addBot" && botParticipant == null) {
            participantFactory = new ParticipantFactory();
            botParticipant = participantFactory.getParticipant("BOT", "DEFAULT-BOT");
        }

        BaseParticipant participant = participants.get(to);

        if(participant != null) {
            participant.receive(from, message);
        }

        if (message.contains("cat") && botParticipant != null) {
            unregister(participants.get(from));
            notifyParticipants();
        }
    }
}
