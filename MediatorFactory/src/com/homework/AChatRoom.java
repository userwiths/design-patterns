package com.homework;

public abstract class AChatRoom {
    public abstract void register(BaseParticipant participant);
    public abstract void unregister(BaseParticipant participant);
    public abstract void send(String from, String to, String message);
}
