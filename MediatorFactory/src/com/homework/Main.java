package com.homework;

public class Main {
    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom();

        ParticipantFactory participantFactory = new ParticipantFactory();
        BaseParticipant georgi = participantFactory.getParticipant("USER", "Georgi");
        BaseParticipant mariq = participantFactory.getParticipant("USER", "Mariq");
        BaseParticipant olaf = participantFactory.getParticipant("USER", "Olaf");

        chatRoom.register(georgi);
        chatRoom.register(mariq);
        chatRoom.register(olaf);

        georgi.send(mariq.getName(), "How's it going?");
        mariq.send(georgi.getName(), "I'm fine thank you!");
        mariq.send(georgi.getName(), "addBot");

        olaf.send("Ivancho", "cat is awesome");
    }
}
